angular.module('kwizoo').factory('ResponseResource', function($resource){
    var resource = $resource('rest/responses/:ResponseId',{ResponseId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});