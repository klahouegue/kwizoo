angular.module('kwizoo').factory('QuestionResource', function($resource){
    var resource = $resource('rest/questions/:QuestionId',{QuestionId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});