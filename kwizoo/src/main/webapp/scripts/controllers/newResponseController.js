
angular.module('kwizoo').controller('NewResponseController', function ($scope, $location, locationParser, ResponseResource , QuestionResource) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.response = $scope.response || {};
    
    $scope.validList = [
        "true",
        " false"
    ];
    
    $scope.questionList = QuestionResource.queryAll(function(items){
        $scope.questionSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.text
            });
        });
    });
    $scope.$watch("questionSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.response.question = {};
            $scope.response.question.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Responses/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        ResponseResource.save($scope.response, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Responses");
    };
});