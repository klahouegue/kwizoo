

angular.module('kwizoo').controller('EditQuestionController', function($scope, $routeParams, $location, QuestionResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.question = new QuestionResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Questions");
        };
        QuestionResource.get({QuestionId:$routeParams.QuestionId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.question);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.question.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Questions");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Questions");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.question.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});