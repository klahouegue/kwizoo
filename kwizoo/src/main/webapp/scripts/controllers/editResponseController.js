

angular.module('kwizoo').controller('EditResponseController', function($scope, $routeParams, $location, ResponseResource , QuestionResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.response = new ResponseResource(self.original);
            QuestionResource.queryAll(function(items) {
                $scope.questionSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.text
                    };
                    if($scope.response.question && item.id == $scope.response.question.id) {
                        $scope.questionSelection = labelObject;
                        $scope.response.question = wrappedObject;
                        self.original.question = $scope.response.question;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            $location.path("/Responses");
        };
        ResponseResource.get({ResponseId:$routeParams.ResponseId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.response);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.response.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Responses");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Responses");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.response.$remove(successCallback, errorCallback);
    };
    
    $scope.validList = [
        "true",  
        " false"  
    ];
    $scope.$watch("questionSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.response.question = {};
            $scope.response.question.id = selection.value;
        }
    });
    
    $scope.get();
});