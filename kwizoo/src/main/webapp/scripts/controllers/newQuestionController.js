
angular.module('kwizoo').controller('NewQuestionController', function ($scope, $location, locationParser, QuestionResource ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.question = $scope.question || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Questions/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        QuestionResource.save($scope.question, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Questions");
    };
});