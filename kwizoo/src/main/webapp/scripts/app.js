'use strict';

angular.module('kwizoo',['ngRoute','ngResource'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/',{templateUrl:'views/landing.html',controller:'LandingPageController'})
      .when('/Questions',{templateUrl:'views/Question/search.html',controller:'SearchQuestionController'})
      .when('/Questions/new',{templateUrl:'views/Question/detail.html',controller:'NewQuestionController'})
      .when('/Questions/edit/:QuestionId',{templateUrl:'views/Question/detail.html',controller:'EditQuestionController'})
      .when('/Responses',{templateUrl:'views/Response/search.html',controller:'SearchResponseController'})
      .when('/Responses/new',{templateUrl:'views/Response/detail.html',controller:'NewResponseController'})
      .when('/Responses/edit/:ResponseId',{templateUrl:'views/Response/detail.html',controller:'EditResponseController'})
      .otherwise({
        redirectTo: '/'
      });
  }])
  .controller('LandingPageController', function LandingPageController() {
  })
  .controller('NavController', function NavController($scope, $location) {
    $scope.matchesRoute = function(route) {
        var path = $location.path();
        return (path === ("/" + route) || path.indexOf("/" + route + "/") == 0);
    };
  });
