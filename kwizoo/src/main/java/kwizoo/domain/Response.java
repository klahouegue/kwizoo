package kwizoo.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Response
 *
 */
@Entity
@Table(name = "t_response")
@XmlRootElement
public class Response implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String text;
	private Boolean valid;
	private Question question;

	public Response() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public Response(String text, Boolean valid) {
		this.text = text;
		this.valid = valid;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Lob
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getValid() {
		return this.valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	@ManyToOne
	@JoinColumn(name = "question_fk")
	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

}
