package kwizoo.util;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import kwizoo.domain.Question;
import kwizoo.domain.Response;


@Singleton
@Startup
public class DBPopulator {
	
	@Inject
	private EntityManager em;
	
	@PostConstruct
	public void createData(){
		
		Response r1 = new Response("0", false);
		Response r2 = new Response("4", false);
		Response r3 = new Response("5", true);
		Response r4 = new Response("1", false);

		Response r5 = new Response("A Java EE Sepecification", false);
		Response r6 = new Response("A famous brand", false);
		Response r7 = new Response("An application server", false);
		Response r8 = new Response("The sixth Amorite king of Babylon", true);
		
		Question q1 = new Question("2+2=?");
		Question q2 = new Question("Who is Hammurabi?");
		
		r1.setQuestion(q1);
		r2.setQuestion(q1);
		r3.setQuestion(q1);
		r4.setQuestion(q1);
		r5.setQuestion(q2);
		r6.setQuestion(q2);
		r7.setQuestion(q2);
		r8.setQuestion(q2);
		
		em.persist(q1);
		em.persist(q2);
		em.persist(r1);
		em.persist(r2);
		em.persist(r3);
		em.persist(r4);
		em.persist(r5);
		em.persist(r6);
		em.persist(r7);
		em.persist(r8);
		
		
		
		
		
		
		
		
	}

}
