package kwizoo.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import kwizoo.domain.Response;

/**
 * 
 */
@Stateless
@Path("/responses")
public class ResponseEndpoint
{
   @PersistenceContext(unitName = "kwizoo")
   private EntityManager em;

   @POST
   @Consumes("application/json")
   public javax.ws.rs.core.Response create(Response entity)
   {
      em.persist(entity);
      return javax.ws.rs.core.Response.created(UriBuilder.fromResource(ResponseEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
   }

   @DELETE
   @Path("/{id:[0-9][0-9]*}")
   public javax.ws.rs.core.Response deleteById(@PathParam("id") Integer id)
   {
      Response entity = em.find(Response.class, id);
      if (entity == null)
      {
         return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
      }
      em.remove(entity);
      return javax.ws.rs.core.Response.noContent().build();
   }

   @GET
   @Path("/{id:[0-9][0-9]*}")
   @Produces("application/json")
   public javax.ws.rs.core.Response findById(@PathParam("id") Integer id)
   {
      TypedQuery<Response> findByIdQuery = em.createQuery("SELECT DISTINCT r FROM Response r LEFT JOIN FETCH r.question WHERE r.id = :entityId ORDER BY r.id", Response.class);
      findByIdQuery.setParameter("entityId", id);
      Response entity;
      try
      {
         entity = findByIdQuery.getSingleResult();
      }
      catch (NoResultException nre)
      {
         entity = null;
      }
      if (entity == null)
      {
         return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
      }
      return javax.ws.rs.core.Response.ok(entity).build();
   }

   @GET
   @Produces("application/json")
   public List<Response> listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult)
   {
      TypedQuery<Response> findAllQuery = em.createQuery("SELECT DISTINCT r FROM Response r LEFT JOIN FETCH r.question ORDER BY r.id", Response.class);
      if (startPosition != null)
      {
         findAllQuery.setFirstResult(startPosition);
      }
      if (maxResult != null)
      {
         findAllQuery.setMaxResults(maxResult);
      }
      final List<Response> results = findAllQuery.getResultList();
      return results;
   }

   @PUT
   @Path("/{id:[0-9][0-9]*}")
   @Consumes("application/json")
   public javax.ws.rs.core.Response update(@PathParam("id") Integer id, Response entity)
   {
      if (entity == null)
      {
         return javax.ws.rs.core.Response.status(Status.BAD_REQUEST).build();
      }
      if (!id.equals(entity.getId()))
      {
         return javax.ws.rs.core.Response.status(Status.CONFLICT).entity(entity).build();
      }
      if (em.find(Response.class, id) == null)
      {
         return javax.ws.rs.core.Response.status(Status.NOT_FOUND).build();
      }
      try
      {
         entity = em.merge(entity);
      }
      catch (OptimisticLockException e)
      {
         return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.CONFLICT).entity(e.getEntity()).build();
      }

      return javax.ws.rs.core.Response.noContent().build();
   }
}
